# SSH

SSH, pour *Secure SHell*, est un protocole de communication sécurisé très poussé qui permet d'interagir avec des machines à distance.

## Installation

Sur le serveur distant, SSH est normalement déjà installé, car c'est ce qui vous permet de vous y connecter. Que ce soit pour un client ou un serveur, l'installation est très simple :

```bash
sudo apt install ssh
```

Ou sur une machine sous Arch Linux :

```bash
sudo pacman -S openssh
```

Voilà, c'est tout. Pour lancer le serveur (inutile pour le serveur d'Animath, il est toujours lancé car indispensable), le service se nomme `sshd` :

```bash
sudo systemctl start sshd
```

Pour faire en sorte que le service soit lancé au démarrage de la machine :

```bash
sudo systemctl enable sshd
```

Le serveur SSH est maintenant lancé, on peut y accéder. Si vous venez de créer un serveur SSH, vous pouvez tester en faisant un `ssh localhost`.

## Connexion

### Connexion basique

Pour se connecter à un serveur SSH distant, la commande est simple, et heureusement étant donné son nombre d'utilisations :

```bash
ssh utilisateur@serveur
```

Cela a pour effet de se connecter au serveur `<serveur>` sous l'identifiant `<utilisateur>`. Il est possible de se restreindre à `ssh serveur` si l'identifiant est le nom d'utilisateur de votre machine personnelle. Si le serveur le permet, on vous demandera ensuite un mot de passe. Une fois rentré, une console s'ouvre à vous : bienvenue dans le cœur du serveur :)

### Connexion sécurisée

Cependant ... Si vous tentez de vous connecter sur le serveur d'Animath, l'accès vous sera refusé, avant même d'avoir rentré le moindre mot de passe :

```bash
ssh ynerant@animath.fr
ynerant@animath.fr: Permission denied (publickey).
```

La raison est simple : le serveur d'Animath n'autorise pas les connexions par mot de passe, jugées trop peu sécurisées. 

Il est en effet préférable d'utiliser un système de chiffrement asymétrique pour s'authentifier, tel que RSA ou ED25519. Il est fortement recommandé de se renseigner sur le fonctionnement général de chiffrement asymétrique. Très simplement, la sécurité est assurée par une paire de clés robustes : l'une est privée et ne doit pas être cédée à qui que ce soit ni être perdue, l'autre est publique et sera partagée au serveur.

Pour générer une paire de clés selon le protocole RSA de 4096 bits (pour qu'elle soit bien robuste), il suffit d'exécuter cette commande sur son propre ordinateur :

```bash
ssh-keygen -b 4096
```

On vous invitera de manière facultative mais recommandée d'entrer une phrase de passe. Votre clé privée (qui doit impérativement avoir `0600` voire `0400` en permissions) se trouve dans `~/.ssh/id_rsa` et la clé publique dans `~/.ssh/id_rsa.pub`.

Il suffit désormais de donner votre clé publique au serveur pour pouvoir vous y connecter. Si vous avez déjà accès au serveur (l'accès via mot de passe est autorisé), vous pouvez exécuter la commande :

```bash
ssh-copy-id -i ~/.ssh/id_rsa.pub utilisateur@serveur
```

Sinon, envoyez votre clé publique (pas la clé privée !) à un administrateur ayant déjà un accès. Il s'occupera de faire exactement ce que fait `ssh-copy-id`, c'est-à-dire de copier le contenu de la clé publique dans le fichier `/home/utilisateur/.ssh/authorized_keys`.

Essayez désormais de vous connecter : `ssh utilisateur@serveur`. Pas de mot de passe (ou éventuellement votre phrase de passe pour pouvoir utiliser votre clé privée) : vous êtes connecté à un terminal sur le serveur distant :)

## Configuration

### Empêcher les connexions par mot de passe

Pour des raisons de sécurité, on veut empêcher les connexions par mot de passe. Cela doit bien sûr se faire *après* que la première clé SSH a été rentrée, sinon l'accès ne sera jamais possible et la machine ne sera plus jamais utilisable sans réinitialisation ou connexion avec un clavier (ce qui serait tout de même dommage).

La configuration du serveur SSH se fait dans le fichier `/etc/ssh/sshd_config`. Pour désactiver l'accès par mot de passe, il suffit d'ajouter ou de décommenter la ligne :

```
PasswordAuthentication no
```

Puis de recharger le service

```
sudo service reload sshd
```

!!! warning ""
	Attention : modifier les paramètres SSH peut être très dangereux. En cas de mauvaise manipulation, c'est l'accès au serveur que vous pouvez perdre, et ce de manière irrévocable. Bon à savoir : la modification des paramètres n'influe pas sur votre connexion SSH actuelle, si vous rechargez la configuration et nous relancez le service. Assurez-vous de garder un terminal ouvert en cas de besoin et de toujours tester vos paramètres de serveur SSH avant de clore toutes les connexions.

NB - Le serveur sur lequel sont hébergés les services d'Animaths n'autorise pas la connexion par mot de passe.

### Configurer ses hôtes SSH

Cette partie concerne le client SSH.

Un des paradigmes du bon Linuxien est de ne jamais taper trop de caractères quand il n'y a pas besoin. On peut donc trouver que taper `ssh utilisateur@serveur` est trop long. Surtout s'il y a des configurations avancées. Pour cela, on peut créer des hôtes SSH.

La configuration se fait dans le fichier `~/.ssh/config`.

Un exemple de configuration :

```
Host animath
	Host toto
	HostName animath.fr
	IdentityFile ~/.ssh/id_rsa
```

De la sorte, il suffit de faire `ssh animath` au lieu de `ssh toto@animath.fr`.

Ce petit tutoriel sur SSH donne toutes les bases pour assurer une administration convenable. SSH permet des opérations bien plus complexes, dont la découverte est laissée libre.
