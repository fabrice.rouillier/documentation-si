# Nginx

Nginx est un logiciel libre de serveur web. Dans certains cas, il peut aussi être utilisé en reverse-proxy (avec Apache en serveur web). Pour le stream, on ne l'utilise que comme serveur web. 

La documentation est disponible [sur le site de Nginx](https://nginx.org/en/docs/). 

## Configuration

Le fichier de configuration (`nginx.conf`) se trouve dans le répertoire `/etc/nginx` : c'est là qu'on décrit l'ensemble des services géré par nginx (les streams en particulier).

```bash
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;
events {
	worker_connections 768;
	# multi_accept on;
}
http {##
        # Basic Settings
        ##
        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;
        server_tokens off;
        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;
        include /etc/nginx/mime.types;
        default_type application/octet-stream;
        ##
        # SSL Settings
        ##
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;
        ##
        # Logging Settings
        ##
        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;
        ##
        # Gzip Settings
        ##
        gzip on;
        ##
        # Virtual Host Configs
        ##
        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;
}

rtmp {
     server {
        ping 30s;
        notify_method get;
        listen 1935;
        chunk_size 4096;
        on_publish http://localhost/rtmp_auth;
        application live1 {
                live on;
                record off;
                hls on;
                hls_path /mnt/www/hls/hls1;
                hls_fragment 1s;
                hls_playlist_length 60s;
                #chaine Animath
                #push rtmp://a.rtmp.youtube.com/suite-de-lurl;
                #chaine Dematherialisation
                push rtmp://a.rtmp.youtube.com/suite-de-lurl;
            	#chaine twitch dematherialisaiton  
                push rtmp://live-cdg.twitch.tv/app/suite-de-lurl;
                #deny play all;
           }
       }
       applicaton live2{
       			live on;
       			#...
       }
		 # configuration des autres chaînes sur le même modèle...
}
```



Ce qui nous intéresse dans le cas du stream est la partie **RTMP** (pour Real Time Messaging Prococol), qui permet la diffusion de flux audio et vidéo pour le streaming entre le serveur (nous) et le client (les navigateurs des personnes qui regardent le stream). RTMP est un protocole propriétaire développé par Adobe Systems. 

Les premières lignes (après `rtmp{`) sont les paramètres qui vont être communs à toutes les chaînes. Le mot-clef `server` permet de déclarer une instance serveur RTMP. Le paramètre `on_publish` est-ce qui nous permet de gérer l'authentification des personnes souhaitant streamer sur nos chaînes. Dès que quelqu'un tentera de streamer sur une chaîne, Nginx va envoyer une requête vers `http://localhost/rtmp_auth` et attendre que cette adresse retourne un résultat (et va autoriser ou non la diffusion en fonction de ce résultat)(*cf. la partie "authentification"*). 

Ensuite sont définies chacune des chaînes, sous la forme `application myapp {....}`. Chaque chaîne peut avoir ses paramètres propres.

- `record off` indique que l'on ne conserve pas les données, elles sont effacées au fur et à mesure.  
- `hls_path` est le répertoire où seront envoyés les données de stream pour la diffusion sur animath.live. Dans notre cas, chaque chaîne à un répertoire dédié (`/mnt/www/hls/hlsX`, ce qui permet de streamer du contenu différent sur plusieurs chaînes en même temps. C'est dans ce répertoire que la page web `www.animath.live/channelX` va aller récupérer les données pour la diffusion (défini dans `/mnt/www/html/channel_X/config.php`).
  - HLS signifie *HTTP Live Streaming*. C'est un protocole de stream basé sur HTTP, qui va segmenter le flux en plusieurs petits fichier (.ts).
- `push rtmp://my/url` permet d'envoyer en parallèle les données ailleurs que sur animath.live (ici, vers nos chaînes Twitch et YouTube). Ainsi, dans le cas de la chaîne 1, on a le stream à 3 endroits en même temps : sur animath.live, sur Twitch, et sur YouTube. 
- La ligne `deny play all` est commentée : sinon, la lecture ne serait possible nulle part. Si on avait `deny play 192.XXX.XXX.XXX` par exemple, la machine avec cette adresse IP ne pourrait pas lire le stream. On peut faire la même chose pour la publication avec `allow|deny publish adresse|subnet|all`. 

Pour créer une nouvelle chaîne, il suffit de rajouter un bloc `application myapp{...}` à la suite, dans le bloc `server{...}`, avec le bon répertoire hls et les bonnes adresses YouTube/Twitch éventuelles.

Attention à bien redémarrer nginx (`systemctl restart nginx`) à chaque modification de la configuration, sinon elle ne sera pas prise en compte.

## L'authentification 

Le paramètre `on_publish` dans la configuration nginx permet d'éviter que n'importe quelle personne disposant de la bonne adresse web puisse streamer. On définit l'adresse `/rtmp_auth` dans le fichier `/etc/nginx/sites-available/default` :

```bash
location /rtmp_auth {
	if ($arg_pwd = 'motdepasse') {
      	return 200;
    }
    	return 401;
}
```
Si le mot de passe est correct, on renvoie `200`, ce qui permettra d'autoriser/poursuivre la publication (donc le stream). Sinon, on renvoie `401`, ce qui refuse/arrête la connexion RTMP : OBS va renvoyer un message d'erreur.

C'est dans ce fichier qu'on peut modifier le mot de passe à utiliser pour le stream.