# Wiki

Un site Wikimedia a été mis en place pour les participants, en 2017. Il n'a pas été mis à jour depuis et n'est pas utilisé, ni voué à être utilisé. Le wiki est en ligne à cette adresse, pour des raisons de conservation des archives : [wiki2.tfjm.org](https://wiki2.tfjm.org).

Une documentation de Wikimedia est accessible à l'adresse : [wikimedia.fr](https://www.wikimedia.fr/)

## Installation

L'installation se fait via l'image Docker officielle :

```yaml
  wiki:
    image: mediawiki
    links:
      - database
    restart: always
    volumes:
      - "/srv/data/wiki/images:/var/www/html/images"
      - "/srv/data/wiki/static:/var/www/html/static"
      - "/srv/data/wiki/skins:/var/www/html/skins"
      - "/srv/data/wiki/extensions:/var/www/html/extensions"
      - "/etc/localtime:/etc/localtime:ro"
      - type: bind
        source: "/srv/data/wiki/LocalSettings.php"
        target: "/var/www/html/LocalSettings.php"
    env_file:
      - /srv/secrets/mediawiki.env
    labels:
      - "traefik.http.routers.wiki.rule=Host(`wiki.example.com`)"
      - "traefik.http.routers.wiki.entrypoints=websecure"
      - "traefik.http.routers.wiki.tls.certresolver=mytlschallenge"
```

Les variables d'environnement à configurer sont :

```
MEDIAWIKI_DB_HOST=database
MEDIAWIKI_DB_USER=
MEDIAWIKI_DB_NAME=
MEDIAWIKI_DB_TYPE=
MEDIAWIKI_DB_PASSWORD=
ServerName=wiki.example.com
```

Le fichier de configuration se trouve ensuite au chemin `/srv/data/wiki/LocalSettings.php`.

La mise à jour se fait simplement via `docker-compose pull wiki && docker-compose up -d wiki`.
