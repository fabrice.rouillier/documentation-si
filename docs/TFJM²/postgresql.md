# PostgreSQL

PostgreSQL est un système de gestion de bases de données relationnelles, fortement utilisé pour stocker des données, à l'image de [MySQL](mysql). Quelques services présents sur le serveur l'utilisent.

Plus d'informations ici : [postgresql.org](https://www.postgresql.org/)

## Installation

L'installation de la base de données se fait comme usuellement via son image Docker officielle :

```yaml
  postgres:
    image: postgres
    volumes:
      - "/srv/data/postgresql:/var/lib/postgresql/data"
    restart: always
    env_file:
      - /srv/secrets/postgres.env
```

Le volume `/var/lib/postgresql/data` doit appartenir à `postgresql:postgresql` (identifiants `999:999`) et avoir pour permissions `0700` pour les dossiers et `0600` pour les fichiers non exécutables (il suffit d'ôter le droit de lecture et d'écriture aux groupes et aux autres).

Le service ne demande qu'une seule variable d'environnement, à savoir le mot de passe root :

```
POSTGRES_PASSWORD=
```

Ce mot de passe doit être robuste. Toutefois, l'accès à la base de données est inaccessible, et n'a lieu de le devenir concernant le serveur d'Animath. Si toutefois vous souhaitez exposer ce serveur à l'extérieur, c'est le port 5432 qui est à exposer.

Pour mettre à jour, il suffit uniquement de mettre à jour l'image : `docker-compose pull postgres && docker-compose up -d postgres`.

Dans les autres services Docker, il vous suffit d'ajouter dans votre configuration :

```
    links:
      - postgresql
```

Et l'hôte `postgresql` sera reconnu par votre service, et il pourra s'y connecter sans problème.

## Communiquer en ligne de commande

Pour ouvrir PostgreSQL en ligne de commande (recommandé), vous pouvez créer et exécuter ce script :

```bash
#!/bin/bash
docker-compose -f /srv/docker/docker-compose.yml exec postgres psql -U postgres $@
```

En le lançant, vous arriverez directement dans un terminal PostgreSQL sous l'utilisateur root.

## Créer une nouvelle base de données

Pour chaque service nécessitant une base de données, on en crée une dédiée, ou plusieurs si un service veut plusieurs bases de données (ce qui est rare car inutile). De plus, on lui crée un compte unique identifié par mot de passe qui dispose de tous les droits sur cette base de données, et seulement celle-ci. On ne veut pas qu'un service puisse avoir accès aux données d'un autre.

```sql
CREATE ROLE service WITH PASSWORD `my_strong_password` LOGIN;
CREATE DATABASE service OWNER service;
```

La base de données `service` est alors créée, accessible depuis le compte `service` avec pour mot de passe `my_strong_password`.
