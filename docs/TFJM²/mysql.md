# MySQL

MySQL est un système de gestion de bases de données relationnelles, fortement utilisé pour stocker des données. La plupart des services présents sur le serveur l'utilisent.

Plus d'informations ici : [mysql.com/fr/](https://www.mysql.com/fr/)

## Installation

L'installation de la base de données se fait comme usuellement via son image Docker officielle :

```yaml
  mysql:
    image: mysql
    command: ["mysqld","--character-set-server=utf8", "--collation-server=utf8_unicode_ci", "--default-authentication-plugin=mysql_native_password"]
    volumes:
      - "/srv/data/mysql:/var/lib/mysql"
    restart: always
    env_file:
      - /srv/secrets/mysql.env
```

Le volume `/var/lib/mysql` doit appartenir à `mysql:mysql` (identifiants `999:999`) et avoir pour permissions `0700` pour les dossiers et `0600` pour les fichiers non exécutables (il suffit d'ôter le droit de lecture et d'écriture aux groupes et aux autres).

Le service ne demande qu'une seule variable d'environnement, à savoir le mot de passe root :

```
MYSQL_ROOT_PASSWORD=
```

Ce mot de passe doit être robuste. Toutefois, l'accès à la base de données est inaccessible, et n'a lieu de le devenir concernant le serveur d'Animath. Si toutefois vous souhaitez exposer ce serveur à l'extérieur, c'est le port 3306 qui est à exposer.

Vous noterez l'ajout de paramètres au lancement du service. Les premiers définissent l'encodage à utiliser par défaut (UTF-8), le dernier permet de configurer le mode d'authentification par défaut au mode classique afin de supporter les plus vieux services qui n'implémentent pas le dernier mode de connexion.

Pour mettre à jour, il suffit uniquement de mettre à jour l'image : `docker-compose pull mysql && docker-compose up -d mysql`.

Dans les autres services Docker, il vous suffit d'ajouter dans votre configuration :

```
    links:
      - mysql
```

Et l'hôte `mysql` sera reconnu par votre service, et il pourra s'y connecter sans problème.

## Communiquer en ligne de commande

Pour ouvrir MySQL en ligne de commande (recommandé), vous pouvez créer et exécuter ce script :

```bash
#!/bin/bash
source /srv/secrets/mysql.env
docker-compose -f /srv/docker/docker-compose.yml exec database mysql --user root --password=$MYSQL_ROOT_PASSWORD $@
```

En le lançant, vous arriverez directement dans un terminal MySQL sous l'utilisateur root. Si vous préférez une interface graphique, tournez-vous vers [PHPMyAdmin](phpmyadmin).

## Créer une nouvelle base de données

Pour chaque service nécessitant une base de données, on en crée une dédiée, ou plusieurs si un service veut plusieurs bases de données (ce qui est rare car inutile). De plus, on lui crée un compte unique identifié par mot de passe qui dispose de tous les droits sur cette base de données, et seulement celle-ci. On ne veut pas qu'un service puisse avoir accès aux données d'un autre.

```sql
CREATE DATABASE service;
CREATE USER 'service' IDENTIFIED BY 'my_strong_password';
GRANT ALL PRIVILEGES ON service.* TO 'service';
```

La base de données `service` est alors créée, accessible depuis le compte `service` avec pour mot de passe `my_strong_password`.
