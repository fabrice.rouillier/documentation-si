# Owncloud

Owncloud est un service de stockage de fichiers. On peut trouver sa documentation [ici](https://doc.owncloud.com/server).

Une instance est accessible ici : [cloud.tfjm.org](https://cloud.tfjm.org).

## Installation

Owncloud s'installe directement depuis son image officielle `owncloud/server`. Sa configuration initiale est simple :

```yaml
  owncloud:
    image: owncloud/server
    links:
      - database
    volumes:
      - /srv/data/owncloud/data:/mnt/data/files
      - /srv/data/owncloud/apps:/mnt/data/custom
      - /srv/data/owncloud/config:/mnt/data/config
      - /etc/localtime:/etc/localtime:ro
    env_file:
      - /srv/secrets/owncloud.env
    restart: always
    labels:
      - "traefik.http.routers.owncloud.rule=Host(`cloud.example.com`)"
      - "traefik.http.routers.owncloud.entrypoints=websecure"
      - "traefik.http.routers.owncloud.tls.certresolver=mytlschallenge"
      - "traefik.http.services.owncloud.loadbalancer.server.port=8080"
```

Le fichier `/srv/secrets/owncloud.env` contenant :

```
OWNCLOUD_DOMAIN=cloud.example.com
OWNCLOUD_DB_TYPE=
OWNCLOUD_DB_NAME=
OWNCLOUD_DB_PREFIX=oc_
OWNCLOUD_DB_USERNAME=
OWNCLOUD_DB_PASSWORD=
OWNCLOUD_DB_HOST=database
OWNCLOUD_MYSQL_UTF8MB4=true
OWNCLOUD_REDIS_ENABLED=false
OWNCLOUD_VOLUME_FILES=/mnt/data/files
```

Il suffit alors de paramétrer l'accès à la base de données.

Les volumes doivent appartenir à `www-data:root`, mais normalement les permissions s'auto-gèrent.

Le fichier de configuration s'auto-génère à partir des variables d'environnement, il n'y a pas à le toucher, mais s'il est bon de le préserver dans un volume afin de ne pas perdre des identifiants secrets.

Une fois le service lancé (`docker-compose up -d owncloud`), il faut se connecter à la page Web pour la configuration initiale avec la création du premier compte.

## Mise à jour

Par prudence, une mise à jour des fichiers et de la base de données s'impose. Il est également recommandé de désactiver toutes les applications tierces.

Ensuite, il suffit de mettre à jour l'image :

```bash
docker-compose pull owncloud
docker-compose up -d owncloud
```

Puis d'exécuter le script de mise à jour post installation :

```bash
docker-compose exec --user www-data owncloud php occ upgrade
```

C'est exactement ce que fait le script placé dans `/srv/upgrade/owncloud` sur le serveur.

Vérifiez ensuite que le service fonctionne à nouveau correctement. Vous pouvez ensuite réactiver les applications tierces.

## Interface en ligne de commande

Owncloud dispose d'un programme nommé `oc` qui permet une interaction en ligne de commande. Pour cela, il suffit de lancer un shell sous l'utilisateur `www-data` :

```bash
docker-compose exec -u www-data owncloud bash
```

Le fichier `occ` se trouve dans `/var/www/owncloud`, qui est le répertoire courant. Il suffit alors d'appeler `php occ list` ou `./occ list` pour voir l'ensemble des commandes disponibles.

Une commande bien pratique par exemple est la commande `files:scan` :

```bash
./occ files:scan <user>
```

Cela permet d'analyser tous les fichiers possédés par `<user>`, situés dans `/mnt/data/files/<user>/files` et de les synchroniser avec la base de données pour les rendre accessibles depuis l'interface Web.
