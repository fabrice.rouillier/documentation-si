# Documentation

Un site a été conçu dans le but de recenser toutes les documentations utiles à l'installation et la mise à jour des services d'Animath, en plus de son administration. Il s'agit en effet de ce site-là :  [doc.tfjm.org](https://doc.tfjm.org). Il est vraissemblable que ce site bouge à l'adresse [doc.animath.fr](https://doc.animath.fr) dans les jours à venir.

Ces pages sont entièrement écrites en Markdown, répertoriées [dans un dépôt Git](https://gitlab.com/animath/si/documentation-si/), puis compilées avec Mkdocs Material, de la même façon que le site [Organiser un TFJM²](organisation). Allez sur cette page pour de plus amples informations sur son installation.
