# Lychee

Lychee est un service de gallerie de photos. Il permet de partager des dossiers de photos, utiles notamment pour partager des souvenirs de finales régionales du TFJM². Une instance est justement disponible à l'adresse [photos.tfjm.org](https://photos.tfjm.org).

La documentation est accessible ici : [https://lycheeorg.github.io/docs/](https://lycheeorg.github.io/docs/).

## Installation

Le service s'installe à partir de son image Docker officielle :

```yaml
  lychee:
    image: lycheeorg/lychee
    links:
      - database
    restart: always
    volumes:
      - "/srv/data/lychee/conf:/conf"
      - "/srv/data/lychee/uploads:/uploads"
      - "/srv/data/lychee/sym:/sym"
      - "/etc/localtime:/etc/localtime:ro"
    env_file:
      - "/srv/secrets/lychee.env
    labels:
      - "traefik.http.routers.lychee.rule=Host(`photos.example.com`)"
      - "traefik.http.routers.lychee.entrypoints=websecure"
      - "traefik.http.routers.lychee.tls.certresolver=mytlschallenge"
```

Les variables d'environnement contenant :

```
PUID=1000
GUID=1000
PHP_TZ=Europe/Paris
DB_DATABASE=mysql
```

Les champs `PUID` et `GUID` doivent contenir respectivement l'identifiant de l'utilisateur et du groupe possédant les données du site. Le champ `PHP_TZ` correspond au fuseau horaire utilisé.

La configuration se fait au moyen du fichier présent dans `/conf/.env`. Les paramètres de connexion à la base de données s'y trouvent.

Au premier lancement du site, un formulaire s'affichera pour pour créer le compte administrateur.

Pour mettre à jour, `docker-compose pull lychee && docker-compose up -d lychee` suffit.
