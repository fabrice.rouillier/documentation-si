# OSTicket

OSTicket est une plateforme pour réceptionner les mails arrivant sur une ou plusieurs adresses mail, sous forme de tickets. Ce service est utilisé notamment au TFJM² ([support.tfjm.org](https://support.tfjm.org)) pour récupérer les mails arrivant à <contact@tfjm.org> et aux Correspondances des Jeunes Mathématicien·nes ([support.correspondances-maths.fr](https://support.correspondances-maths.fr)) pour ceux arrivant à <contact@correspondances-maths.fr>.

Plus d'informations sur son fonctionnement : [osticket.com](https://osticket.com/).

## Installation

L'installation se fait via une image Docker tierce, présente néanmoins sur DockerHub : [github.com/CampbellSoftwareSolutions/docker-osticket](https://github.com/CampbellSoftwareSolutions/docker-osticket/)

La configuration dans le fichier `docker-compose.yml` reste simple :

```yaml
  osticket:
    image: campbellsoftwaresolutions/osticket
    links:
      - database
    restart: always
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
    env_file:
      - /srv/secrets/osticket.env
    labels:
      - "traefik.http.routers.osticket.rule=Host(`support.example.com`)"
      - "traefik.http.routers.osticket.entrypoints=websecure"
      - "traefik.http.routers.osticket.tls.certresolver=mytlschallenge"
```

On note l'absence de volume monté, à part `/etc/localtime` qui permet de recevoir des mails à l'heure et non une à deux heures dans le futur.

Si vous désirez installer des plugins, il y a néanmoins besoin de monter le dossier distant `/data/upload/include/plugins`.

Les variables d'environnement à instancier sont nombreuses :

```
INSTALL_NAME=			# My Helpdesk
INSTALL_EMAIL=			# helpdesk@example.com
INSTALL_URL=			# http://localhost:8080/
ADMIN_FIRSTNAME=		# Admin
ADMIN_LASTNAME=			# User
ADMIN_EMAIL=			# admin@example.com
ADMIN_USERNAME=			# ostadmin
ADMIN_PASSWORD=			# Admin1
MYSQL_PREFIX=			# ost_
MYSQL_HOST=database		# mysql
MYSQL_PORT=				# 3306
MYSQL_DATABASE=			# osticket
MYSQL_USER=				# osticket
MYSQL_PASSWORD=			# 
SMTP_HOST=				# localhost
SMTP_PORT=				# 25
SMTP_FROM=				# 
SMTP_TLS=				# 1
SMTP_TLS_CERTS=			# /etc/ssl/certs/ca-certificates.crt
SMTP_USER=				# 
SMTP_PASSWORD=			# 
CRON_INTERVAL=			# 5
INSTALL_SECRET=			# 
INSTALL_CONFIG=			# /data/upload/include/ost-sampleconfig.php
```

La valeur après le `#` correspond à la valeur par défaut (y compris si elle est vide).

Si `SMTP_FROM` n'est pas spécifié, la valeur de `SMTP_USER` sera utilisée.

De nombreux paramètres ne sont pas obligatoires, notamment tous ceux commençant par `INSTALL_`, `ADMIN_` et `SMTP_` qui peuvent être configurés dès le premier lancement du service et ensuite mémorisés en base de données, rendant ensuite inutile ces variables d'environnement. Remplir ces champs permet de se dispenser de la première phase d'installation.

Cependant, une fois installé, il est **indispensable** de remplir la variable `INSTALL_SECRET`. Elle est générée une unique fois et est utile pour chiffrer et déchiffrer les données (si elles doivent être chiffrées).

Pour la remplir, une fois l'installation effectuée, ouvrez le fichier de configuration et récupérez la valeur de `SECRET_SALT` :

```
docker cp docker_support_1:/data/upload/include/ost-config.php - | grep -a 'SECRET_SALT'
```

Où `docker_support_1` est à remplacer par le nom du conteneur. Cette clé doit alors être placée immédiatement en valeur de `INSTALL_SECRET` dans les variables d'environnement.

Le service est désormais correctement installé et est prêt à être utilisé.

## Mise à jour

La mise à jour est très simple : il suffit d'exécuter `docker-compose pull osticket && docker-compose up -d osticket`, puis de se rendre dans l'interface administrateur pour finaliser la mise à jour.
