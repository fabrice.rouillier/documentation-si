# Limesurvey

Limesurvey est un service très puissant pour créer des sondages à envoyer aux participants.

On peut en trouver une instance ici : [survey.tfjm.org](https://survey.tfjm.org)

La documentation du service est accessible ici : [www.limesurvey.org/fr/](https://www.limesurvey.org/fr/)

## Installation

### Le Dockerfile

L'installation est assurée par une image Docker : [github.com/martialblog/docker-limesurvey](https://github.com/martialblog/docker-limesurvey)

On suppose que le dépôt est cloné dans `/srv/sources/limesurvey`, que les données sont dans `/srv/data` et les variables d'environnement dans `/srv/secrets/limesurvey.env`. La configuration Dockerfile :

```yaml
  limesurvey:
    build: /srv/sources/limesurvey/4.0/apache/
    links:
      - database
    volumes:
      - type: bind
        source: "/srv/data/limesurvey/application/config/security.php"
        target: "/var/www/html/application/config/security.php"
      - "/srv/data/limesurvey/uploads:/app/upload"
      - "/etc/localtime:/etc/localtime:ro"
    env_file:
      - /srv/secrets/limesurvey.env
    restart: always
    labels:
      - "traefik.http.routers.survey.rule=Host(`survey.example.com`)"
      - "traefik.http.routers.survey.entrypoints=websecure"
      - "traefik.http.routers.survey.tls.certresolver=mytlschallenge"
```

On a besoin de définir les variables d'environnement suivantes :

```
DB_TYPE=
DB_HOST=
DB_NAME=
DB_USERNAME=
DB_PASSWORD=
DB_TABLE_PREFIX=
ADMIN_USER=
ADMIN_NAME=
ADMIN_EMAIL=
ADMIN_PASSWORD=
PUBLIC_URL=survey.example.com
```

Les variables préfixées par `DB_` concernent la connexion à la base de données, celles par `ADMIN_` la connexion à l'interface administrateur pour le premier compte. La variable `PUBLIC_URL` contient l'adresse principale à laquelle est accessible le service.

### Les volumes

Le dossier `/app/upload` contient les fichiers envoyés lors de sondages, ce dossier doit alors être un volume, possédé par `www-data:www-data`.

Le volume `/etc/localtime` sert uniquement à dire quel est le fuseau horaire utilisé.

Le premier volume est plus complexe. Pour chiffrer les données, tel que le mot de passe du serveur mail ou les données de certains formulaires, Limesurvey utilise une paire de clés de chiffrement. Afin de pouvoir déchiffrer les informations, les clés ne doivent pas être perdues. Elles sont stockées dans `/var/www/html/application/config.php`.

Cependant, ce fichier est généré automatiquement lors du premier lancement et ne doit pas être créé à la main. Il n'est à l'heure actuelle pas possible de monter ce fichier en volume et qu'il soit rempli automatiquement. Lors de la première installation, voici la procédure à suivre :

 * Commenter les 3 lignes liant le fichier `security.php`
 * Lancer Limesurvey : `docker-compose up -d survey`
 * Copier le fichier généré : `docker cp docker_limesurvey_1:/var/www/html/application/config/security.php /srv/data/limesurvey/application/config/security.php`
 * S'assurer des bonnes permissions : `sudo chown -R www-data:www-data /srv/data/limesurvey/*`
 * Décommenter les 3 lignes dans le fichier `docker-compose.yml` et relancer le service

De la sorte, on a généré une paire de clés auxquelles il ne faut plus toucher.

Limesurvey est désormais prêt à l'emploi.

## La mise à jour

La mise à jour de Limesurvey est facilité par l'image. Un script est prévu dans `/srv/upgrade/limesurvey.sh`, qui effectue les opérations suivantes :

```bash
cd /srv/sources/limesurvey
git pull
docker-compose up -f /srv/docker/docker-compose.yml -d --build limesurvey
docker-compose -f /srv/docker/docker-compose.yml exec limesurvey php application/commands/console.php updatedb
```

On met alors à jour le dépôt Git, on reconstruit l'image Docker puis on met à jour la base de données si besoin.
