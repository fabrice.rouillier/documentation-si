# Gitea

Gitea est un serveur Git auto-hébergé qui a l'avantage d'être très léger. Sa documentation est accessible ici : [docs.gitea.io](https://docs.gitea.io/fr-fr/).

Un instance est disponible pour le TFJM² à l'adresse [git.tfjm.org](https://git.tfjm.org), qui permet d'héberger entre autres les sources des problèmes du TFJM² et des Correspondances, mais aussi des logiciels faits maison pour le TFJM².

## Installation

L'installation de Gitea est facilitée par son image Docker officielle `gitea/gitea`. Le `docker-compose.yml` :

```yaml
  gitea:
    image: gitea/gitea
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
      - "/srv/data/gitea:/data"
    restart: always
    ports:
      - "2222:2222"
    env_file:
      - "/srv/secrets/gitea.env"
    labels:
      - "traefik.http.routers.gitea.rule=Host(`git.example.com`)"
      - "traefik.http.routers.gitea.entrypoints=websecure"
      - "traefik.http.services.gitea.loadbalancer.server.port=3000"
      - "traefik.http.routers.gitea.tls.certresolver=mytlschallenge"
```

Et ses variables d'environnement :

```
APP_NAME=Gitea
RUN_MODE=prod
DOMAIN=git.example.com
ROOT_URL=https://git.example.com/
SSH_DOMAIN=git.example.com
SSH_PORT=2222
SSH_LISTEN_PORT=2222
DISABLE_SSH=false
HTTP_PORT=3000
LFS_START_SERVER=false
DB_TYPE=
DB_HOST=
DB_NAME=
DB_USER=
DB_PASSWD=
INSTALL_LOCK=true
```

Ces variables d'environnement servent uniquement à générer la configuration initiale. Ne pas les renseigner lancera un installateur lors de la première connexion au site Web.

Le champ `DB_HOST` doit être renseigné au format `hôte:port`, par exemple `database:3306` ou `postgres:5432`.

Une fois le service lancé, la configuration se trouve dans `/srv/data/gitea/gitea/conf/app.ini`. Elle peut être modifiée librement, un `docker-compose restart gitea` suffit à recharger la configuration. Dès lors, les variables d'environnement ne sont plus nécessaires.

Pour mettre à jour le service, il suffit d'exécuter `docker-compose pull gitea && docker-compose up -d gitea` : les migrations de données se feront toutes seules.

## Connexion SSH

Il est possible de se connecter au serveur Gitea par connexion SSH. La configuration ci-dessus utilise le port 2222 au lieu du traditionnel port 22, car celui-ci est déjà utilisé par la connexion au serveur principal. L'ajout d'une option `-p 2222` ou `-P 2222` suffit en général.
