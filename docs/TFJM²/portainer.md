# Portainer

Portainer est une interface Web pour gérer les containers Docker, pour les non initiés à la ligne de commande. Ceci ne doit pas être utilisé pour créer de nouveaux conteneurs (ou éventuellement sur une machine de développement pour déployer des conteneurs à la volée), mais peut servir pour leur maintenance.

Une instance est disponible à l'adresse : [portainer.tfjm.org](https://portainer.tfjm.org).

La documentation peut se trouver ici : [portainer.io](https://www.portainer.io/).

## Installation

Portainer s'installe via son image Docker officielle :

```yaml
  portainer:
    image: portainer/portainer
    restart: always
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
      - "/srv/data/portainer:/data"
      - "/etc/localtime:/etc/localtime:ro"
    labels:
      - "traefik.http.routers.portainer.rule=Host(`portainer.example.com`)"
      - "traefik.http.routers.portainer.entrypoints=websecure"
      - "traefik.http.routers.portainer.tls.certresolver=mytlschallenge"
      - "traefik.http.services.portainer.loadbalancer.server.port=9000"
```

Portainer a besoin d'accéder au socket de Docker pour pouvoir intéragir avec le serveur Docker.

Portainer se met simplement à jour en récupérant la dernière image : `docker-compose pull portainer && docker-compose up -d portainer`.
