# Tirages

Plateforme de tirages des problèmes. Peut-être obsolète ? À voir en 2021. N'a jamais pu servir, malgré une tentative en 2019.

Lien : [tirages.tfjm.org](https://tirages.tfjm.org)

Il est inutile de chercher à l'installer pour le moment, avant de savoir quoi en faire. Une documentation arrivera en temps voulu.
