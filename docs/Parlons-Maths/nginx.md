# Nginx

Nginx est un locgiciel permettant la création de serveurs Web simples à très complexes, et ce de façon optimisée.
C'est ce service qui permet d'afficher le site vitrine de Parlons-Maths ([parlons-maths.fr](https://parlons-maths.fr/),
à partir de sources HTML simples.

Plus de détails sur la documentation [nginx.org/en/docs/](https://nginx.org/en/docs/).

## Installation

On utilise l'image Docker officielle de Nginx, construite sur une image Alpine Linux dans un souci de légèreté.
Une installation basique est très simple :

```yaml
  nginx:
    image: nginx:alpine
    restart: always
    volumes:
      - "/srv/data/nginx/site:/usr/share/nginx/html:ro"
    labels:
      - "traefik.http.routers.nginx.rule=Host(`example.com`)"
      - "traefik.http.routers.nginx.entrypoints=websecure"
      - "traefik.http.routers.nginx.tls.certresolver=mytlschallenge"
```

De la sorte, le dossier `/srv/data/nginx/site` sera directement exposé sur votre site Web (en lecture seule ici
car l'écriture n'est pas nécessaire).

Si vous voulez installer votre propre configuration Nginx, les dossiers `/etc/nginx/sites-available`
et `/etc/nginx/sites-enabled` n'existent pas, vous devez remplacer le fichier par défaut en ajoutant un
volume ressemblant à `/srv/data/nginx/config/default.conf:/etc/nginx/conf.d/nginx:ro`.
Vous pourrez ensuite monter les volumes de votre choix.

!!! info "Bon à savoir"
	Les fichiers de logs redirigent directement vers la sortie standard, ie. dans les logs de Docker.
    Il suffit d'exécuter `docker-compose logs nginx` pour les trouver.

